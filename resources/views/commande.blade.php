<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Commandes') }}
        </h2>
    </x-slot>

    <div class="container">
        <p>Filter By :</p>
        <form action="{{ route('commandes.index') }}">
            <div class="row">
                <div class="col-3">
                    <label class="form-label" for="client">client</label>
                    <input class="form-input" type="text" name="client">
                </div>
                <div class="col-3">
                    <label class="form-label" for="etat">etat_id</label>
                    <input class="form-input" type="text" name="etat">
                </div>
                <div class="col-3">
                    <label class="form-label" for="etat">date min</label>
                    <input class="form-input" type="date" name="date_min">
                </div>
                <div class="col-3">
                    <label class="form-label" for="etat">date max</label>
                    <input class="form-input" type="date" name="date_max">
                </div>
            </div>
            <input type="submit" value="Filter">
        </form>

        <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <th>Id</th>
                <th>client name</th>
                <th>etat</th>
                <th colspan="3">Actions</th>
            </thead>
            <tbody>
                @foreach ($commandes as $commande)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th scope="row"
                            class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $commande->id }}
                        </th>
                        <td class="px-6 py-4">
                            {{ $commande->client->nom }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $commande->etat->intitule }}
                        </td>
                        <td class="px-6 py-4 text-right flex gap-2">
                            <a href="#">
                                <img src="{{ asset('/details.png') }}">
                            </a>
                            <a class="btn btn-success" href="#">
                                <img src="{{ asset('/change.png') }}">
                            </a>
                            <form action="" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger" type="submit"
                                    onclick="return confirm('voulez-vous supprimer cette categorie?')">
                                    <img src="{{ asset('/delete.png') }}">
                                </button>

                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</x-app-layout>
