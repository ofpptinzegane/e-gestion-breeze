<?php

namespace App\Http\Controllers;

use App\Models\Etat;
use App\Models\Client;
use App\Models\Commande;
use Illuminate\Http\Request;
use App\Models\LigneCommande;
use App\Http\Controllers\Controller;

class CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       $commandes=Commande::all();
       
        return view("commande",compact('commandes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view("commandes.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $client = [
            "nom"=>$request->nom,
            "prenom"=>$request->prenom,
            "ville"=>$request->ville,
            "adresse"=>$request->adresse,
            "tele"=>$request->tele,
        ];
        $produits = session()->get("user.produits");

        $client = Client::create($client);
        $etat = Etat::create(["intitule"=>"EN ATTENT DE CONFIRMATION","description"=>"EN ATTENT DE CONFIRMATION"]);
        $command = Commande::create(["client_id"=>$client->id,"etat_id"=>$etat->id]);
        foreach($produits as $prod){
            LigneCommande::create([
                "commande_id"=>$command->id,
                "produit_id"=>$prod["id"],
                "qte"=>$prod["qnt"],
                "prix"=>$prod["prix_u"]

            ]);
        }

        return "Your Commande is Fully Noted";


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
